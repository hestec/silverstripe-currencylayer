<?php

class CurrencyLayerExtension extends DataExtension {

    public function CurrencyConvert($amount = null, $from = null, $to = null){

        if (is_numeric($amount) && strlen($to) == 3 && strlen($from) == 3 && ctype_alpha($to) && ctype_alpha($from)) {

            // the rates in the db are all related to USD, so if $from is not USD we have to convert to USD first
            if ($from != "USD"){
                if ($fromrate = CurrencyLayerRate::get()->filter("CLKey", "USD".$from)->last()){
                    $amount = $amount / $fromrate->Rate;
                }else{
                    return false;
                    exit;
                }
            }

            if ($rate = CurrencyLayerRate::get()->filter("CLKey", "USD".$to)->last()){

                $converted = number_format($amount * $rate->Rate, 2, '.', '');

                return $to." ".$converted;

            }else{
                return false;
            }

        }else{
            return false;
        }

    }

}

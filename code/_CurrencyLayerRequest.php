<?php

class CurrencyLayerRequest {

    public function getCurrencyLayerConnection($endpoint = "live")
    {

        $apikey = Config::inst()->get('CurrencyLayer', 'apikey');

        /*if (!isset($source)){
            $source = Config::inst()->get('CurrencyLayer', 'source_currency');
        }*/

        $service = new RestfulService("http://apilayer.net/api/".$endpoint."?access_key=".$apikey, 0);
        //$service->httpHeader("Content-Type: application/json");

        //Debug::message("http://apilayer.net/api/".$endpoint."?access_key=".$apikey);

        return $service;

    }
}
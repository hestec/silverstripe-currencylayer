<?php
class CurrencyFromCountry extends DataObject {

    private static $db = array(
        'CountryCode' => 'Varchar(2)',
        'CurrencyCode' => 'Varchar(3)'
    );

}
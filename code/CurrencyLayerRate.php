<?php
class CurrencyLayerRate extends DataObject {

    private static $db = array(
        'CLKey' => 'Varchar(6)',
        'Rate' => 'Decimal(13,6)'
    );

}